package com.neostra.presentation;

public class CartList {
    private String name;
    private int image_url;

    private double price;

    public CartList(String name, int image_url, double price) {
        this.name = name;
        this.image_url = image_url;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage_url() {
        return image_url;
    }

    public void setImage_url(int image_url) {
        this.image_url = image_url;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


}
