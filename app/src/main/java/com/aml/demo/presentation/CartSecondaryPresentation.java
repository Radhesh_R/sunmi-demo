package com.aml.demo.presentation;

import android.app.Presentation;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aml.demo.Listner.PresentationListner;
import com.aml.demo.models.ProductList;
import com.aml.demo.utils.BasePresentation;
import com.aml.demo.utils.IMPlayListener;
import com.aml.demo.utils.IMPlayer;
import com.aml.demo.utils.MPlayer;
import com.aml.demo.utils.MPlayerException;
import com.aml.demo.utils.MinimalDisplay;
import com.aml.demo.utils.Singleton;
import com.aml.demo.R;
import com.aml.demo.adapter.CartAdapterSecondary;
import com.aml.demo.adapter.BurgerAdapterPrimary;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CartSecondaryPresentation extends Presentation implements PresentationListner {
    private Context mContext;
    private DisplayManager mDisplayManager;
    private Display[] displays;
    public static int positon = 0;
    private Handler myHandle = new Handler(Looper.getMainLooper());
    SharedPreferences shared;
    private String text;
    private CartAdapterSecondary cartAdapterSecondary;
    private BurgerAdapterPrimary dataAdapt;
    private final String TAG = "SUNMI";
    RecyclerView recyclerView;
    List<ProductList> list;
    int amount, discount, total_items, total;
    public VideoView video;
    private MPlayer player;
    public TextView sub_mount_txt, discount_amount_txt, item_count_txt, total_amount_txt, vat_value, item_value, empty, present_order_note_value;
    String order_notes;
    private FrameLayout container;
    public  FrameLayout frame;

    public CartSecondaryPresentation(Context context, Display display) {
        super(context, display);
        mContext = context;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_presentation);

        sub_mount_txt = (TextView) findViewById(R.id.sub_amount);
        discount_amount_txt = (TextView) findViewById(R.id.discount_amount);
        item_value = (TextView) findViewById(R.id.item_value);
        total_amount_txt = (TextView) findViewById(R.id.total_value);
        empty = (TextView) findViewById(R.id.empty);
        vat_value = (TextView) findViewById(R.id.vat_value);

 frame= (FrameLayout) findViewById(R.id.frame);
        present_order_note_value = (TextView) findViewById(R.id.present_order_note_value);

        empty.setVisibility(View.GONE);

        //   path="android.resource://" + getContext().getPackageName() + "/" + R.raw.promo_two;

        SharedPreferences mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = mSecurePrefs.edit();

        Gson gson = new Gson();
        list = new ArrayList();
        Type listType = new TypeToken<List<ProductList>>() {
        }.getType();
        list = gson.fromJson(mSecurePrefs.getString("product_list", null), listType);
        Log.d("Cart List", gson.toJson(list).toString());
        if (list.size() == 0) {
            empty.setVisibility(View.VISIBLE);

        } else if (list.size() > 0) {
            empty.setVisibility(View.GONE);

        }
        video = (VideoView) findViewById(R.id.mPlayerView);

        //  container = (FrameLayout) findViewById(R.id.playerContainer);

        //  initPlayer();
        Log.d(TAG, "onCreate: ------------>" + (player == null));

        recyclerView = (RecyclerView) findViewById(R.id.recycler_cart_presentation);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        cartAdapterSecondary = new CartAdapterSecondary(mContext, list);


        recyclerView.setAdapter(cartAdapterSecondary);
        cartAdapterSecondary.updateList();
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.smoothScrollToPosition(cartAdapterSecondary.getItemCount() + 1);
            }
        });


    }

    /*@Override
    public void onSelect(boolean isShow) {
        if(player != null){
            if (isShow) {
                try {
                    player.setSource(path, positon);
                    player.onResume();
                } catch (MPlayerException e) {
                    e.printStackTrace();
                    myHandle.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                player.setSource(path, 0);
                                player.onResume();
                            } catch (MPlayerException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }, 3000);
                    Log.d(TAG, "onCreate: ---------->" + e.getMessage());
                }
            }else {
                if(player.getPosition()!=0)
                    positon = player.getPosition();
            }
        }
    }

*/
    @Override
    public void onPresentation(List<ProductList> productList) {
        list.clear();
        list.addAll(productList);
        cartAdapterSecondary = new CartAdapterSecondary(mContext, list);
        recyclerView.setAdapter(cartAdapterSecondary);
        cartAdapterSecondary.updateList();
    }

    public void updatePresentationList(List<ProductList> productList) {
        list.clear();
        list.addAll(productList);
        cartAdapterSecondary = new CartAdapterSecondary(mContext, list);
        recyclerView.setAdapter(cartAdapterSecondary);
        cartAdapterSecondary.updateList();


    }


    @Override
    public void onDisplayRemoved() {
        super.onDisplayRemoved();
//        player.onDestroy();
    }

}