package com.aml.demo.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.aml.demo.Listner.OnCountIncreaseListner;
import com.aml.demo.Listner.PresentationListner;
import com.aml.demo.R;
import com.aml.demo.adapter.BurgerAdapterPrimary;

import com.aml.demo.models.ProductList;
import com.aml.demo.presentation.CartSecondaryPresentation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class MainActivity extends AppCompatActivity implements PresentationListner, OnCountIncreaseListner {
    private DisplayManager mDisplayManager;
    private Display[] displays;


    private CartSecondaryPresentation presentation1 = null;
    private ImageView cart_icon;
    SharedPreferences mSecurePrefs;
    OnCountIncreaseListner onCountIncreaseListner;

    ArrayList<ProductList> productList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


       // burgerfrag = new BurgerFragment();

    /*    mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = mSecurePrefs.edit();

        Gson gson = new Gson();
        List<ProductList> list = new ArrayList();
        Type listType = new TypeToken<List<ProductList>>() {
        }.getType();


        String json = gson.toJson(list);
        Log.d("List", json);
        editor.putString("product_list", json);
        editor.apply();
        androidx.fragment.app.FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();*/

        SharedPreferences mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = mSecurePrefs.edit();

        Gson gson = new Gson();
        List<ProductList> list = new ArrayList();
        Type listType = new TypeToken<List<ProductList>>() {
        }.getType();



        String json = gson.toJson(list);
        Log.d("List",json);
        editor.putString("product_list", json);
        editor.apply();



productList = new ArrayList<>();
        productList.add(new ProductList(1,"Whiskey King Burger",10.00,1, R.drawable.bgr6,10.00,10.00,true,0,""));
        productList.add(new ProductList(2,"Chargrilled Burger with Roquefort Cheese",10.00,1, R.drawable.bgr6,20.00,20.00,true,0,""));
        productList.add(new ProductList(3,"Dyer’s Deep-Fried Burger",10.00,1, R.drawable.bgr6,30.00,30.00,true,0,""));
        productList.add(new ProductList(4," Raw Steak Tartare Burger",10.00,1, R.drawable.bgr6,40.00,40.00,true,0,""));
        productList.add(new ProductList(5,"Double Patty Cheeseburger",10.00,1, R.drawable.bgr6,50.00,50.00,true,0,""));
        productList.add(new ProductList(6,"Le Pigeon Burger. Le Pigeon – Portland",10.00,1, R.drawable.bgr6,60.00,50.00,true,0,""));
        productList.add(new ProductList(7,"The Original Burger. Louis' Lunch",10.00,1, R.drawable.bgr6,70.00,50.00,true,0,""));
        productList.add(new ProductList(8,"The Company Burger",10.00,1, R.drawable.bgr6,50.00,80.00,true,0,""));
        productList.add(new ProductList(9,"The Original Burger",10.00,1, R.drawable.bgr6,50.00,90.00,true,0,""));
        productList.add(new ProductList(10,"Buckhorn Burger",10.00,1, R.drawable.bgr6,50.00,30.00,true,0,""));
        productList.add(new ProductList(11,"Kobe Burger",10.00,1, R.drawable.bgr6,50.00,70.00,true,0,""));
        productList.add(new ProductList(12,"Black Label Burger",10.00,1, R.drawable.bgr6,80.00,50.00,true,0,""));
        productList.add(new ProductList(13,"Steamed Cheeseburger",10.00,1, R.drawable.bgr6,50.00,50.00,true,0,""));


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(MainActivity.this,2);
        recyclerView.setLayoutManager(layoutManager);

        BurgerAdapterPrimary adapter = new BurgerAdapterPrimary(MainActivity.this, productList);
        recyclerView.setAdapter(adapter);


       // loadFragment(new CartFragmentPrimary());
        mDisplayManager = (DisplayManager) MainActivity.this.getSystemService(Context.DISPLAY_SERVICE);
        displays = mDisplayManager.getDisplays(null);
        //   String file="android.resource://" + getContext().getPackageName() + "/" + R.raw.promo_two";
       // String filename = "android.resource://" + MainActivity.this.getPackageName() + "/raw/promo_two.mp4";
        presentation1 = new CartSecondaryPresentation(MainActivity.this, displays[1]);
        presentation1.show();
        presentation1. video.setVideoURI(Uri.parse("android.resource://" + MainActivity.this.getPackageName() + "/" + R.raw.promo_two));
        presentation1. video.start();
        presentation1.video.requestFocus();
        presentation1. video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

    }


    @Override
    public void onPresentation(List<ProductList> productList) {
        //presentationListner=(PresentationListner)context;
    }

    @Override
    public void onBackPressed() {


            super.onBackPressed();
        mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = mSecurePrefs.edit();
        editor.clear();
        editor.commit();

        editor.apply();



    }

@Override
    protected void onDestroy() {
        super.onDestroy();

    presentation1.dismiss();

    }

    @Override
    public void onCountIncreseResponse(List<ProductList> mList) {

        double mTotal=0;
        double mDiscount =0;
        double subTotal =0;
        double totalQty=0;
        double vat=100;
        int count=0;

        SharedPreferences mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = mSecurePrefs.edit();

        Gson gson = new Gson();


        String json = gson.toJson(mList);
        Log.d("List", json);
        editor.putString("product_list", json);
        editor.apply();

        DecimalFormat decim = new DecimalFormat("0.00");
        for(int i=0; i<mList.size();i++){
            ProductList obj = mList.get(i);
            totalQty = totalQty +obj.getQty();
            mDiscount = mDiscount + obj.getDiscount();
            subTotal = subTotal + (obj.getPrice()*obj.getQty());
            count=obj.getCount();
        }
        mTotal = subTotal-mDiscount+vat;
        presentation1.sub_mount_txt.setText(String.valueOf(decim.format(subTotal)));
        presentation1.discount_amount_txt.setText(String.valueOf(decim.format(mDiscount)));
        presentation1.total_amount_txt.setText(String.valueOf("AED"+" "+decim.format(mTotal)));
        presentation1.vat_value.setText(String.valueOf(decim.format(vat)));
        presentation1.item_value.setText(String.valueOf(count));

        presentation1.updatePresentationList(mList);

    }

    @Override
    protected void onPause() {
        super.onPause();
        presentation1.video.pause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        presentation1.video.start();
    }
}