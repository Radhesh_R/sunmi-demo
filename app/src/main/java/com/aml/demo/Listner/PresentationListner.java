package com.aml.demo.Listner;

import com.aml.demo.models.ProductList;

import java.util.List;

public interface  PresentationListner {
    void onPresentation(List<ProductList> productList);
}
