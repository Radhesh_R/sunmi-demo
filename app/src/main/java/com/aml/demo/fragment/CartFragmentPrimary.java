package com.aml.demo.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aml.demo.Listner.PresentationListner;
import com.aml.demo.activity.MainActivity;
import com.aml.demo.presentation.CartSecondaryPresentation;

import com.aml.demo.models.ProductList;
import com.aml.demo.Listner.OnCountIncreaseListner;
import com.aml.demo.R;
import com.aml.demo.adapter.CartAdapterPrimary;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class CartFragmentPrimary extends Fragment implements OnCountIncreaseListner {
    String order_notes;
    private PresentationListner presentationListner;
    SharedPreferences shared;
    private DisplayManager mDisplayManager;
    private Display[] displays;
    private CartAdapterPrimary cartMainAdapter;
    TextView sub_amount_value, discountAmount, vat_value, total, item_count, order_note_value;
    SurfaceHolder surfaceHolder;
    private Context context;
    private CartSecondaryPresentation presentation1 = null;
    View view;
    List<ProductList> list;
    List<ProductList> productList;
    ProductList list_note = new ProductList();
    private Button order_note;
    FrameLayout frame_main;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDisplayManager = (DisplayManager) getActivity().getSystemService(Context.DISPLAY_SERVICE);
        displays = mDisplayManager.getDisplays(null);

        //   String file="android.resource://" + getContext().getPackageName() + "/" + R.raw.promo_two";
        String filename = "android.resource://" + getContext().getPackageName() + "/raw/promo_two.mp4";
        presentation1 = new CartSecondaryPresentation(getContext(), displays[1]);
        presentation1.show();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_primary_screen__cart, container, false);
        SharedPreferences mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = mSecurePrefs.edit();

        Gson gson = new Gson();
        list = new ArrayList();

        Type listType = new TypeToken<List<ProductList>>() {
        }.getType();
        list = gson.fromJson(mSecurePrefs.getString("product_list", null), listType);
        Log.d("Cart List", gson.toJson(list).toString());
//        presentationListner.onPresentation(list);
        sub_amount_value = (TextView) view.findViewById(R.id.sub_amount);
        discountAmount = (TextView) view.findViewById(R.id.discount_amount);
        vat_value = (TextView) view.findViewById(R.id.vat_value);
        total = (TextView) view.findViewById(R.id.total);
        item_count = (TextView) view.findViewById(R.id.item_count);
        frame_main=(FrameLayout)view.findViewById(R.id.frame_main);
        order_note = view.findViewById(R.id.order_note);
        order_note_value = view.findViewById(R.id.order_note_value);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        order_notes = sharedPreferences.getString("order_notes", "");
        order_note_value.setText(order_notes);
        if(order_notes.equals("")){
            presentation1.frame.setVisibility(View.GONE);
            frame_main.setVisibility(View.GONE);
        }else if(!order_notes.equals("")){
            presentation1.frame.setVisibility(View.VISIBLE);
            frame_main.setVisibility(View.VISIBLE);
        }
        presentation1.present_order_note_value.setText(order_notes);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.primary_recycler_view);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        cartMainAdapter = new CartAdapterPrimary(getContext(), list, this::onCountIncreseResponse);

        recyclerView.setAdapter(cartMainAdapter);
        cartMainAdapter.updateList();

        order_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotDialog(getContext());
            }
        });


        return view;

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // this.presentationListner=(PresentationListner)context;
    }

    @Override
    public void onCountIncreseResponse(List<ProductList> mList) {
        double mTotal = 0;
        double mDiscount = 0;
        double subTotal = 0;
        double totalQty = 0;
        double vat = 100;
        int count = 0;

        SharedPreferences mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = mSecurePrefs.edit();

        Gson gson = new Gson();


        String json = gson.toJson(mList);
        Log.d("List", json);
        editor.putString("product_list", json);
        editor.apply();

        DecimalFormat decim = new DecimalFormat("0.00");
        for (int i = 0; i < mList.size(); i++) {
            ProductList obj = mList.get(i);
            totalQty = totalQty + obj.getQty();
            mDiscount = mDiscount + obj.getDiscount();
            subTotal = subTotal + (obj.getPrice() * obj.getQty());
            count = obj.getCount();
        }
        mTotal = subTotal - mDiscount + vat;
        total.setText(String.valueOf(decim.format(mTotal) + " " + "AED"));
        discountAmount.setText(String.valueOf(decim.format(mDiscount) + " " + "AED"));
        item_count.setText(String.valueOf(count));
        sub_amount_value.setText(String.valueOf(decim.format(subTotal) + " " + "AED"));


        presentation1.sub_mount_txt.setText(String.valueOf(decim.format(subTotal)));
        presentation1.discount_amount_txt.setText(String.valueOf(decim.format(mDiscount)));
        presentation1.total_amount_txt.setText(String.valueOf("AED" + " " + decim.format(mTotal)));
        presentation1.vat_value.setText(String.valueOf(decim.format(vat)));
        presentation1.item_value.setText(String.valueOf(count));

        presentation1.updatePresentationList(mList);
    }

    private void showForgotDialog(Context c) {
        final EditText taskEditText = new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Notes")
                .setMessage("Enter Your Notes")
                .setView(taskEditText)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        order_notes = String.valueOf(taskEditText.getText());
                        if(order_notes.equals("")){
                            presentation1.frame.setVisibility(View.GONE);
                            frame_main.setVisibility(View.GONE);
                        }else if(!order_notes.equals("")){
                            presentation1.frame.setVisibility(View.VISIBLE);
                            frame_main.setVisibility(View.VISIBLE);
                        }
                                    order_note_value.setText(order_notes);


                        presentation1.present_order_note_value.setText(order_notes);
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("order_notes", order_notes);
                        editor.commit();



                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.getWindow().setLayout(100, 100);
        dialog.show();
    }
   /* @Override
    public void onDestroy() {
        super.onDestroy();
        presentation1.dismiss();

    }*/

}