package com.aml.demo.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.aml.demo.models.ProductList;
import com.aml.demo.R;
import com.aml.demo.fragment.CartFragmentPrimary;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class BurgerAdapterPrimary extends RecyclerView.Adapter<BurgerAdapterPrimary.ViewHolder> {
    // private ArrayList<ProductList> productList;
    private Context context;

    private List<ProductList> productList;


    public BurgerAdapterPrimary(Context context, List<ProductList> productList) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public BurgerAdapterPrimary.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BurgerAdapterPrimary.ViewHolder viewHolder, final int position) {
        final ProductList myListData = productList.get(position);

        viewHolder.amount.setText("AED" + " " + productList.get(position).getPrice());
        viewHolder.name.setText(productList.get(position).getName());
        //  Picasso.with(context).load(android.get(i).getAndroid_image_url()).into(viewHolder.img_android);

        //   Picasso.with(context).load(R.drawable.a).into(viewHolder.img_android);
        Glide.with(context)
                .load(productList.get(position).getImage_url())
                .into(viewHolder.img_android);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ProductList myListData = productList.get(position);
                if (myListData.isAdded()) {
                    myListData.setAdded(false);

                    productList.set(position, myListData);
                    notifyDataSetChanged();

                    SharedPreferences mSecurePrefs = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor editor = mSecurePrefs.edit();

                    Gson gson = new Gson();
                    List<ProductList> list = new ArrayList();
                    Type listType = new TypeToken<List<ProductList>>() {
                    }.getType();
                    list = gson.fromJson(mSecurePrefs.getString("product_list", "[]"), listType);
                    list.add(myListData);

                    String json = gson.toJson(list);
                    Log.d("List", json);
                    editor.putString("product_list", json);
                    editor.apply();
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    Fragment myFragment = new CartFragmentPrimary();
                    activity.getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_cart, myFragment).commit();


                }

            }
        });
    }



    private int grandTotal(List<ProductList> myListData) {
        int totalPrice = 0;
        for (int i = 0; i < productList.size(); i++) {
            totalPrice += productList.get(i).getPrice();
        }
        return totalPrice;
    }


    @Override
    public int getItemCount() {
        return productList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView amount, name;
        private ImageView img_android;

        private ProductList mEditStampData;

        public ViewHolder(View view) {
            super(view);
            amount = (TextView) view.findViewById(R.id.amount);
            name = (TextView) view.findViewById(R.id.name);
            img_android = (ImageView) view.findViewById(R.id.img_android);


        }


    }
}