package com.aml.demo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aml.demo.models.ProductList;
import com.aml.demo.R;


import java.text.DecimalFormat;
import java.util.List;

public class CartAdapterSecondary extends RecyclerView.Adapter<CartAdapterSecondary.ViewHolder> {
    // private ArrayList<ProductList> productList;
    private Context context;
    private List<ProductList> productLists;
    private String value;

    public CartAdapterSecondary(Context context, List<ProductList> productLists) {
        this.productLists = productLists;
        this.context = context;

    }



    @Override
    public CartAdapterSecondary.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CartAdapterSecondary.ViewHolder viewHolder, final int position) {
        final ProductList productListss = productLists.get(position);
        int subTotal =0;

        DecimalFormat decim = new DecimalFormat("0.00");
       // holder.comm_code.setText(String.valueOf(position+1));
        viewHolder.item_name.setText(productLists.get(position).getName());
        viewHolder.discount_value.setText(String.valueOf("AED"+" "+decim.format(productLists.get(position).getDiscount())));
        if(productLists.get(position).getNotes().equals("")){

            viewHolder.relative_note.setVisibility(View.GONE);
        }else if(!productLists.get(position).getNotes().equals("")){

            viewHolder.relative_note.setVisibility(View.VISIBLE);
        }
            viewHolder.notes_value.setText(productLists.get(position).getNotes());



      //  viewHolder.notes_value.setText(productLists.get(position).getNotes());
        viewHolder.quantity_value.setText(String.valueOf(productLists.get(position).getQty()));
        viewHolder.serial_value.setText(String.valueOf(position+1));
        viewHolder.rate.setText(String.valueOf("AED"+" "+decim.format(productLists.get(position).getRate())));


        double total = subTotal + (productLists.get(position).getPrice()*productLists.get(position).getQty());
        viewHolder.price.setText("AED"+" "+String.valueOf(decim.format(total)));
/*

        Glide.with(context)
                .load(productLists.get(position).getImage_url())
                .into(viewHolder.img_android);
*/


    }



    @Override
    public int getItemCount() {
        return productLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView serial_no,item_name, discount_value,quantity_value,rate,price,serial_value,notes_value;
      //  private ImageView img_android;
      private RelativeLayout relative_note;
        public ViewHolder(View view) {
            super(view);

            item_name = (TextView) view.findViewById(R.id.item_name);
            discount_value = (TextView) view.findViewById(R.id.discount_value);
            notes_value= (TextView) view.findViewById(R.id.notes_value);
            rate = (TextView) view.findViewById(R.id.rate);
            price = (TextView) view.findViewById(R.id.price);
            quantity_value= (TextView) view.findViewById(R.id.quantity_value);
            serial_value= (TextView) view.findViewById(R.id.serial_value);
            relative_note= view.findViewById(R.id.relative_note);
        }


    }

    public void updateList(){
        notifyDataSetChanged();

    }
}
