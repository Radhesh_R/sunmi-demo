package com.aml.demo.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.text.Transliterator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.aml.demo.models.ProductList;
import com.aml.demo.Listner.OnCountIncreaseListner;
import com.aml.demo.R;
import com.aml.demo.fragment.CartFragmentPrimary;
import com.bumptech.glide.Glide;


import java.text.DecimalFormat;
import java.util.List;

public class CartAdapterPrimary extends RecyclerView.Adapter<CartAdapterPrimary.ViewHolder> {
    int totalPrice = 0;
    int discount;
    int total_amount;
    int quantity_resp;
    private final Context context;
    private List<ProductList> productList;
    OnCountIncreaseListner onCountIncreaseListner;
    private CartFragmentPrimary fragment;
    String notes;


    public CartAdapterPrimary(Context context, List<ProductList> productList, OnCountIncreaseListner onCountIncreaseListner) {
        this.productList = productList;
        this.context = context;
        this.onCountIncreaseListner = onCountIncreaseListner;


    }


    @Override
    public CartAdapterPrimary.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart_main, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CartAdapterPrimary.ViewHolder viewHolder, final int position) {
        final ProductList myListData = productList.get(position);
        DecimalFormat decim = new DecimalFormat("0.00");
        viewHolder.rate.setText(String.valueOf(productList.get(position).getRate()));

        double total = productList.get(position).getRate() * quantity_resp;
        productList.get(position).setCount(productList.size());
        onCountIncreaseListner.onCountIncreseResponse(productList);
        viewHolder.item_name.setText(productList.get(position).getName());
        viewHolder.discount_value.setText(String.valueOf(decim.format(productList.get(position).getDiscount()) + " " + "AED"));
        if (productList.get(position).getNotes().equals("")) {

            viewHolder.relative_note.setVisibility(View.GONE);
        } else if (!productList.get(position).getNotes().equals("")) {

            viewHolder.relative_note.setVisibility(View.VISIBLE);
        }
        viewHolder.notes_value.setText(productList.get(position).getNotes());


        Glide.with(context)
                .load(productList.get(position).getImage_url())
                .into(viewHolder.img_android);
        viewHolder.qunatity.setText("" + myListData.getQty());
        viewHolder.rate.setText(String.valueOf("AED" + " " + decim.format(myListData.getQty() * myListData.getPrice())));
        viewHolder.decrease.setOnClickListener(v -> {
            if (myListData.getQty() > 1) {
                ProductList productListObj = productList.get(position);
                productListObj.setQty(productListObj.getQty() - 1);
                productList.set(position, productListObj);

                notifyDataSetChanged();
                onCountIncreaseListner.onCountIncreseResponse(productList);
            }
        });

        viewHolder.increase.setOnClickListener(v -> {
            ProductList productListObj = productList.get(position);
            productListObj.setQty(productListObj.getQty() + 1);
            productList.set(position, productListObj);


            notifyDataSetChanged();
            onCountIncreaseListner.onCountIncreseResponse(productList);
        });
        viewHolder.note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotDialog(context, viewHolder, position);


            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    private void showForgotDialog(Context c, ViewHolder v, int p) {
        final EditText taskEditText = new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Notes")
                .setMessage("Enter Your Notes")
                .setView(taskEditText)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        notes = String.valueOf(taskEditText.getText());
                        ProductList productListObj = productList.get(p);
                        productListObj.setNotes(notes);
                        v.notes_value.setText(productListObj.getNotes());

                        notifyDataSetChanged();


                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.getWindow().setLayout(100, 100);
        dialog.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView rate, total, item_name, discount_value, qunatity, notes_value;
        private ImageView img_android;
        private ImageView decrease, increase;
        private RelativeLayout relative_note;
        private Button note;

        public ViewHolder(View view) {
            super(view);
            qunatity = view.findViewById(R.id.qunatity);
            rate = view.findViewById(R.id.rate);
            total = view.findViewById(R.id.total);
            item_name = view.findViewById(R.id.item_name);
            discount_value = view.findViewById(R.id.discount_value);
            img_android = view.findViewById(R.id.img_android);
            decrease = view.findViewById(R.id.decrease);
            increase = view.findViewById(R.id.increase);
            note = view.findViewById(R.id.note);
            notes_value = view.findViewById(R.id.notes_value);
            relative_note = view.findViewById(R.id.relative_note);

        }


    }

    public void updateList() {
        notifyDataSetChanged();
    }


}
