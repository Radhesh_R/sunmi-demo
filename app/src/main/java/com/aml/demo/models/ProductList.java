package com.aml.demo.models;

public class ProductList {
    private int serail_no;
    private String name;
    private double discount;
    private int qty=1;
    private int image_url;
private int count;
    private double rate;
    private double price;
    private boolean added=true;
    private String notes;


    public ProductList(int serail_no,String name,double discount, int qty,int image_url,double rate, double price,boolean added,int count,String notes) {
        this.name = name;
        this.discount = discount;
        this.qty = qty;
        this.image_url = image_url;
        this.rate = rate;
        this.price = price;
        this.added=added;
        this.count=count;
        this.notes=notes;

    }

    public ProductList() {

    }


    public String getName() {
        return name;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage_url() {
        return image_url;
    }

    public void setImage_url(int image_url) {
        this.image_url = image_url;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSerail_no() {
        return serail_no;
    }

    public void setSerail_no(int serail_no) {
        this.serail_no = serail_no;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
